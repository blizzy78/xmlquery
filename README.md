Reads XML from standard input and outputs XML of nodes selected by an XQuery expression.

```
Usage:
  xmlquery [OPTIONS]

Application Options:
  -e, --expression=     XQuery expression to select nodes from the input document
  -c, --contents-only   Print only the contents of selected nodes
      --color           Use colored output
      --no-color        Don't use colored output
  -C, --no-child-nodes  Don't output child nodes of selected nodes

Help Options:
  -h, --help            Show this help message
```
