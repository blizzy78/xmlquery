SOURCES = $(shell find src -type f -name '*\.go')
BINARY = xmlquery

.PHONY: all update-deps clean

all: bin/$(BINARY)

bin/$(BINARY): $(SOURCES)
	mkdir -p bin
	cd src && go build -o ../bin/$(BINARY)

run: bin/$(BINARY)
	bin/$(BINARY)

update-deps:
	cd src && go mod tidy

clean:
	rm -rf bin/*
