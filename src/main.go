package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"os"
	"strings"

	"github.com/antchfx/xmlquery"
	"github.com/jessevdk/go-flags"
	"github.com/mattn/go-isatty"
	"github.com/ttacon/chalk"
)

type options struct {
	Expression string `required:"true" long:"expression" short:"e" description:"XQuery expression to select nodes from the input document"`
	ContentsOnly bool `long:"contents-only" short:"c" description:"Print only the contents of selected nodes"`
	Color bool `long:"color" description:"Use colored output"`
	NoColor bool `long:"no-color" description:"Don't use colored output"`
	NoChildNodes bool `long:"no-child-nodes" short:"C" description:"Don't output child nodes of selected nodes"`
}

func main() {
	var opts options
	optsParser := flags.NewParser(&opts, flags.Default)
	_, err := optsParser.Parse()
	if err != nil {
		os.Exit(1)
	}

	if len(opts.Expression) == 0 {
		fmt.Print("no expression specified\n")
		os.Exit(1)
	}

	if opts.Color && opts.NoColor {
		fmt.Print("cannot use -color and -no-color together\n")
		os.Exit(1)
	}

	doc, err := xmlquery.Parse(os.Stdin)
	exitOnError(err)

	isTerminal := isatty.IsTerminal(os.Stdout.Fd()) || isatty.IsCygwinTerminal(os.Stdout.Fd())
	useColor := (isTerminal || opts.Color) && !opts.NoColor

	defer func() {
		if err2 := recover(); err2 != nil {
			exitOnError(fmt.Errorf("error in expression: %s", err2.(error).Error()))
		}
	}()

	nodes := xmlquery.Find(doc, opts.Expression)
	for _, node := range nodes {
		fmt.Print(outputXML(node, !opts.ContentsOnly, useColor, !opts.NoChildNodes))
		fmt.Println()
	}
}

func exitOnError(err error) {
	if err != nil {
		fmt.Print(err.Error())
		fmt.Println()
		os.Exit(1)
	}
}

func outputXML(n *xmlquery.Node, self bool, color bool, recursive bool) string {
	var buf bytes.Buffer
	if self {
		outputXMLToBuffer(&buf, n, color, recursive)
	} else {
		for n := n.FirstChild; n != nil; n = n.NextSibling {
			outputXMLToBuffer(&buf, n, color, recursive)
		}
	}

	return buf.String()
}

func outputXMLToBuffer(buf *bytes.Buffer, n *xmlquery.Node, color bool, recursive bool) {
	if n.Type == xmlquery.TextNode || n.Type == xmlquery.CommentNode {
		xml.EscapeText(buf, []byte(strings.TrimSpace(n.Data)))
		return
	}
	if n.Type == xmlquery.DeclarationNode {
		if color {
			buf.WriteString(chalk.Green.String())
		}
		buf.WriteString("<?")
		buf.WriteString(n.Data)
	} else {
		if color {
			buf.WriteString(chalk.Magenta.String())
		}
		if n.Prefix == "" {
			buf.WriteString("<" + n.Data)
		} else {
			buf.WriteString("<" + n.Prefix + ":" + n.Data)
		}
	}
	if color {
		buf.WriteString(chalk.Reset.String())
	}

	for _, attr := range n.Attr {
		if color {
			buf.WriteString(chalk.Yellow.String())
		}
		if attr.Name.Space != "" {
			buf.WriteString(fmt.Sprintf(` %s:%s="`, attr.Name.Space, attr.Name.Local))
		} else {
			buf.WriteString(fmt.Sprintf(` %s="`, attr.Name.Local))
		}
		if color {
			buf.WriteString(chalk.Reset.String())
			buf.WriteString(chalk.Cyan.String())
		}
		buf.WriteString(attr.Value)
		if color {
			buf.WriteString(chalk.Reset.String())
			buf.WriteString(chalk.Yellow.String())
		}
		buf.WriteString(`"`)
	}
	if color {
		buf.WriteString(chalk.Reset.String())
	}

	if n.Type == xmlquery.DeclarationNode {
		if color {
			buf.WriteString(chalk.Green.String())
		}
		buf.WriteString("?>")
	} else {
		if color {
			buf.WriteString(chalk.Magenta.String())
		}
		buf.WriteString(">")
	}
	if color {
		buf.WriteString(chalk.Reset.String())
	}

	if recursive {
		for child := n.FirstChild; child != nil; child = child.NextSibling {
			outputXMLToBuffer(buf, child, color, recursive)
		}
	}
	if n.Type != xmlquery.DeclarationNode {
		if color {
			buf.WriteString(chalk.Magenta.String())
		}
		if n.Prefix == "" {
			buf.WriteString(fmt.Sprintf("</%s>", n.Data))
		} else {
			buf.WriteString(fmt.Sprintf("</%s:%s>", n.Prefix, n.Data))
		}
		if color {
			buf.WriteString(chalk.ResetColor.String())
		}
	}
}
