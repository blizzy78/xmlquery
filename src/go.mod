module xmlquery

require (
	github.com/antchfx/xmlquery v0.0.0-20181204011708-431a9e9e7c44
	github.com/antchfx/xpath v0.0.0-20181208024549-4bbdf6db12aa // indirect
	github.com/jessevdk/go-flags v1.4.0
	github.com/mattn/go-isatty v0.0.4
	github.com/ttacon/chalk v0.0.0-20160626202418-22c06c80ed31
	golang.org/x/net v0.0.0-20181207154023-610586996380 // indirect
	golang.org/x/text v0.3.0 // indirect
)
